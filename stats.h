/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.h 
 * @brief Appplication Assignment for Week 1
 *
 * Functions declarations of stats.c
 *
 * @author Ignacio Mehle
 * @date 31 oct 2020
 *
 */
#ifndef __STATS_H__
#define __STATS_H__

/* Prototypes and Function Comments */

void sort_array(unsigned char *array, unsigned int size_array);
/**
 * @brief Sorts array from max to min
 *
 * Uses 2 for loops to sort the array
 *
 * @param <array> <pointer to the array>
 * @param <size_array> <Number of items in the array>
 *
 * @return <Returns pointer to array, now sorted>
 */

void print_array(unsigned char *array, unsigned int size_array);
/**
 * @brief Prints sorted array
 *
 * The items are printed in rows of 10.
 * I'm currently working in a better algorithm for printing.
 *
 * @param <array> <Array pointer>
 * @param <size_array> <Number of items in the array>
 *
 * @return <Void>
 */

void print_statistics(unsigned char *array, unsigned int size_array);
/**
 * @brief Prints array statistcs
 *
 * Obtains the max, min, mean and median values of the given array
 * Calls an individual function for each value.
 *
 * @param <array> <Array pointer>
 * @param <size_array> <Number of items in the array>
 *
 * @return <Void>
 */

unsigned char find_median(unsigned char *array, unsigned int size);
/**
 * @brief Finds median value of the array
 *
 * Different algorithms for even/odd array size
 *
 * @param <array> <Array pointer>
 * @param <size> <Number of items in the array>
 *
 * @return <Char with integer casted median value>
 */

unsigned char find_mean(unsigned char *array, unsigned int size);
/**
 * @brief Finds mean value of the array
 *
 * @param <array> <Array pointer>
 * @param <size> <Number of items in the array>
 *
 * @return <Char with integer casted mean value>
 */

unsigned char find_maximum(unsigned char *array, unsigned int size);
/**
 * @brief Finds maximum value of the array
 *
 * Stores the max value in an iteration
 *
 * @param <array> <Array pointer>
 * @param <size> <Number of items in the array>
 *
 * @return <Char with integer casted maximum value>
 */

unsigned char find_minimum(unsigned char *array, unsigned int size);
/**
 * @brief Finds minimum value of the array
 *
 * Stores the min value in an iteration
 *
 * @param <array> <Array pointer>
 * @param <size> <Number of items in the array>
 *
 * @return <Char with integer casted minimum value>
 */

#endif /* __STATS_H__ */