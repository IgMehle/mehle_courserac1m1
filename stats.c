/******************************************************************************
 * Copyright (C) 2017 by Alex Fosdick - University of Colorado
 *
 * Redistribution, modification or use of this software in source or binary
 * forms is permitted as long as the files maintain this copyright. Users are 
 * permitted to modify this and use it to learn about the field of embedded
 * software. Alex Fosdick and the University of Colorado are not liable for any
 * misuse of this material. 
 *
 *****************************************************************************/
/**
 * @file stats.c 
 * @brief Application Assignment of Week 1
 *
 * This program sorts a given array and prints
 * statistical values
 *
 * @author Ignacio Mehle
 * @date 31 oct 2020
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};

  /* Other Variable Declarations Go Here */
  /* Statistics and Printing Functions Go Here */
  printf("\nHello!  This is a program to print statistics\n");
  printf("and sort from max value to min value the given array.\n\n");
  sort_array(test, SIZE);
  print_array(test, SIZE);
  print_statistics(test, SIZE);

}

/* Add other Implementation File Code Here */

void sort_array(unsigned char *array, unsigned int size_array){
  unsigned char aux = 0;
  for (int i = 0; i < size_array; i++){
    for (int j = 0; j < (size_array-1); j++){
      if( array[j] < array[j+1]){
        aux = array[j];
        array[j] = array[j+1];
        array[j+1] = aux;
      }
    }
  }
}

void print_array(unsigned char *array, unsigned int size_array){
  int count = 0, i = 0;
  printf("Sorted array:\n");
  while( count < size_array ){
    if(i < 10){
      printf("%i\t",array[count]);
      i++;
    }
    else{
      printf("\n%i\t",array[count]);
      i = 1;
    }
    count++;
  }
}

void print_statistics(unsigned char *array, unsigned int size_array){
  unsigned char max = find_maximum(array, size_array);
  unsigned char min = find_minimum(array, size_array);
  unsigned char mean = find_mean(array,size_array);
  unsigned char median = find_median(array,size_array);
  printf("\n\nMax. value of the array: %i", max);
  printf("\nMin. value of the array: %i", min);
  printf("\nMean value of the array: %i", mean);
  printf("\nMedian of the array: %i\n\n", median);
}

unsigned char find_maximum(unsigned char *array, unsigned int size){
  unsigned char aux = 0;
  for (int i = 0; i < size; i++){
    if (array[i]>aux){
      aux = array[i];
    }
  }
  return aux;
}

unsigned char find_minimum(unsigned char *array, unsigned int size){
  unsigned char aux = array[0];
  for (int i = 1; i < size; i++){
    if (array[i] < aux){
      aux = array[i];
    }
  }
  return aux;
}

unsigned char find_mean(unsigned char *array, unsigned int size){
  unsigned int sum = 0;
  for (int i = 0; i<size; i++ ){
    sum += (unsigned int)(array[i]);
  }
  return (unsigned char)(sum/size);
}

unsigned char find_median(unsigned char *array, unsigned int size){
  if ( (size%2) == 0){
    unsigned char aux1 = array[size/2];
    unsigned char aux2 = array[(size/2)+1];
    return (unsigned char)((aux1+aux2)/2);
  }
  else {
    return (unsigned char)(array[(size+1)/2]);
  }
}